import React, {Component} from 'react'
import {observer} from 'mobx-react'
import AppReactTable from './components/ReactTable'
import FixedDataTable from './components/FiexdDataTable'
import ServerSideData from './components/ServerSideData'
import ReactVirtualized from './components/ReactVirtualized'
import UsageExample from './components/Map/UsageExample'
import AntdTable from './components/AntdTable'
import Gridlist from './components/Gridlist'
import FrozenColumnsExample from './components/FrozenColumnsExample'
import ReactMapGL from './components/Map/ReactMapGl'

@observer
class App extends Component {
  render() {

    return (
      <div className='App'>
        <UsageExample/>
      </div>
    )
  }
}

export default App
