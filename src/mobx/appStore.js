import {observable, action} from 'mobx'

let store = observable({
  data: [],
  point:[]
})
store.change = action((name, value) => {
  store[name] = value
})

export default store