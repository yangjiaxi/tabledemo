import React from 'react'
import ReactTable from 'react-table'
import 'react-table/react-table.css'
import axios from 'axios'
import {Icon, Button} from 'antd'

const token = 'v=1;id=58d88f4f9e62ccfcb6377806;n=2jizb6fe;expiry=2017-10-20T02:25:17Z;hmac=gG1LUHyY86OoBKIZTLKyGM39eheHkSOdv_l79AJ5G0g='

const url = 'https://bird.coolhei.com/manager/api/v2/device/'
const _battery_voltage = v => {
  if (v < 4) {
    return (
      <Icon type='link'/>
    )
  } else {
    return (
      <Icon type="check-circle"/>
    )
  }
}
const setting = v => {
  return (
    <div>
      <Button type='primary' onClick={e => {
        console.log(v)
      }}>设备配置</Button>
      <Button>生物信息</Button>
    </div>

  )
}
const _columns = [{
  Header: '设备编号',
  accessor: 'mark',
  filterable: true

}, {
  Header: '最后通信时间',
  accessor: 'updated_at'
}, {
  Header: '设置',
  accessor: 'id',
  Cell: props => setting(props.value)
},]

class ServerSideData extends React.Component {
  constructor() {
    super()
    this.state = {
      data: [],
      pages: null,
      loading: false,
      count: null
    }
    this.fetchData = this.fetchData.bind(this)

  }

  fetchData(state, instance) {
    const o = {
      'x-druid-authentication': token,

    }
    console.log(state, instance)

    this.setState({loading: true})

    const headers = Object.assign(o, {
      'x-result-limit': state.pageSize,
      'x-result-offset': state.page * state.pageSize
    })
    axios.get(url, {headers})
      .then(res => {
        const count = res.headers['x-result-count']
        const data = res.data
        const pages = Math.floor(count / state.pageSize) + 1
        this.setState({count, data, pages, loading: false})
      })

  }

  render() {
    return (
      <ReactTable
        data={this.state.data} // should default to []
        pages={this.state.pages} // should default to -1 (which means we don't know how many pages we have)
        columns={_columns}
        style={{height: 800, width: 700}}
        loading={this.state.loading}
        manual // informs React Table that you'll be handling sorting and pagination server-side
        onFetchData={this.fetchData}
      />
    )
  }
}

export default ServerSideData


/*
*
*
*
*
*
*
*  <ReactTable
        data={this.state.data} // should default to []
        pages={this.state.pages} // should default to -1 (which means we don't know how many pages we have)
        loading={this.state.loading}
        manual // informs React Table that you'll be handling sorting and pagination server-side
        onFetchData={this.fetchData}
      />*/
