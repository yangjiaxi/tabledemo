import React from 'react'
import axios from 'axios'
import L from 'leaflet'
import store from '../../mobx/appStore'
import {observer} from 'mobx-react'
//https://webrd0{s}.is.autonavi.com/appmaptile?lang=zh_cn&size=1&scale=1&style=8&x={x}&y={y}&z={z}
//`https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=${accessToken}
let mymap = null
const accessToken = 'pk.eyJ1IjoieWFuZ2ppYXhpIiwiYSI6ImNqNzl0NTN0MzA2bmwzMnFtcXJ4MDNkbzMifQ.O2IKwcIyEBlcxzcLSukBKA'

@observer
class UsageExample extends React.Component {
  constructor() {
    super()
    this.state = {
      point: []
    }
    axios.get('/point.json')
      .then(res => {
        const point = []
        try {

          store.change('point', point)
          console.log(store.point)
        }
        catch (err) {
        }

      })
      .catch(err => {
        console.log(err)
      })
  }

  componentDidUpdate() {
    mymap = L.map('mapid').setView([39.75621, -104.99404], 13);
    L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
      attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="http://mapbox.com">Mapbox</a>',
      maxZoom: 18,
      id: 'mapbox.satellite',
      accessToken,
    }).addTo(mymap)


  }

  render() {
    const point = store.point
    console.log(point)
    return (
      <div id='mapid' style={{height: 600, width: 800}}></div>
    )
  }
}

export default UsageExample

