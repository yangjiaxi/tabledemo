import React from 'react'
import MapGL from 'react-map-gl'

const MAPBOX_TOKEN = 'pk.eyJ1IjoieWFuZ2ppYXhpIiwiYSI6ImNqNzl0NTN0MzA2bmwzMnFtcXJ4MDNkbzMifQ.O2IKwcIyEBlcxzcLSukBKA'

class Map extends React.Component {
  constructor() {
    super()

    this.state = {
      mapStyle: '',
      viewport: {
        latitude: 37.805,
        longitude: -122.447,
        zoom: 15.5,
        bearing: 0,
        pitch: 0,
        width: 500,
        height: 500
      }
    }

  }

  componentDidMount() {
    window.addEventListener('resize', this._resize)
    this._resize()
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this._resize)
  }

  _resize = () => {
    this.setState({
      viewport: {
        ...this.state.viewport,
        width: this.props.width || window.innerWidth,
        height: this.props.height || window.innerHeight
      }
    })
  }

  _onViewportChange = viewport => this.setState({viewport})


  render() {
    const {viewport, mapStyle} = this.state
    return (
      <MapGL
        {...viewport}
        mapStyle={mapStyle}
        onViewportChange={this._onViewportChange}
        mapboxApiAccessToken={MAPBOX_TOKEN}>
      </MapGL>
    )
  }
}

export default Map