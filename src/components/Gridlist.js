import React from 'react'
import {Grid, List, ScrollSync} from 'react-virtualized'
import axios from 'axios'
import 'react-virtualized/styles.css';

const list = [
  ['Brian Vaughn', 'Software Engineer', 'San Jose', 'CA', 95125 /* ... */],
  [1, 2, 3, 4],
  [1, 2, 3, 4],
  // And so on...
]

function cellRenderer({
                        columnIndex, // Horizontal (column) index of cell
                        isScrolling, // The Grid is currently being scrolled
                        isVisible,   // This cell is visible within the grid (eg it is not an overscanned cell)
                        key,         // Unique key within array of cells
                        parent,      // Reference to the parent Grid (instance)
                        rowIndex,    // Vertical (row) index of cell
                        style        // Style object to be applied to cell (to position it);
                                     // This must be passed through to the rendered cell element.
                      }) {
  console.log({columnIndex, isScrolling, isVisible, parent, key, rowIndex, style})
  return (
    <div
      key={key}
      style={style}
    >
      {list[rowIndex][columnIndex]}
    </div>
  )
}

class GridList extends React.Component {

  constructor() {
    super()
    this.state = {
      data: []
    }
    axios.get('/test.json')
      .then(res => {
        console.log(res.data)
        this.setState({data: res.data})
      })
  }


  render() {
    return (
      <Grid
        cellRenderer={cellRenderer}
        columnCount={list[0].length}
        columnWidth={300}
        height={300}
        rowCount={list.length}
        rowHeight={30}
        width={300}
      />)
  }
}

export default GridList
