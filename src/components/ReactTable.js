import React, {Component} from 'react'
import {observer} from 'mobx-react'
import store from '../mobx/appStore'
import axios from 'axios'
import ReactTable from 'react-table'
import 'react-table/react-table.css'
import {Icon, Button} from 'antd'

const test_data = [{
  name: 'Tanner Linsley',
  age: 26,
  friend: {
    name: 'Jason Maurer',
    age: 23,
  }
}]
const columns = [{
  Header: 'Name',
  accessor: 'name' // String-based value accessors!
}, {
  Header: 'Age',
  accessor: 'age',
  Cell: props => <span className='number' style={{color: '#e84f5c'}}>{props.value}</span> // Custom cell components!
}, {
  id: 'friendName', // Required because our accessor is not a string
  Header: 'Friend Name',
  accessor: d => d.friend.name // Custom value accessors!
}, {
  Header: props => <span>Friend Age</span>, // Custom header components!
  accessor: 'friend.age'
}]
const _battery_voltage = v => {
  if (v < 4) {
    return (
      <Icon type='link'/>
    )
  } else {
    return (
      <Icon type="check-circle"/>
    )
  }
}
const setting = v => {
  return (
    <div>
      <Button type='primary' onClick={e => {
        console.log(v)
      }}>设备配置</Button>
      <Button>生物信息</Button>
    </div>

  )
}

const _columns = [{
  Header: '设备编号',
  accessor: 'mark',
  filterable:true

}, {
  Header: '最后通信时间',
  accessor: 'updated_at'
}, {
  Header: '电量',
  accessor: 'battery_voltage',
  Cell: props => _battery_voltage(props.value)
}, {
  Header: '设置',
  accessor: 'id',
  Cell: props => setting(props.value)
},

]


@observer
class AppReactTable extends Component {
  constructor() {
    super()
    axios.get('/test.json')
      .then(res => {
        store.change('data', res.data)
      })
  }

  render() {

    return (
      <div>
        <ReactTable
          data={store.data}
          columns={_columns}
          showPaginationBottom={false}
          style={{height: 800, width: 700}}
          defaultPageSize={50}


        />
      </div>
    )
  }
}

export default AppReactTable
