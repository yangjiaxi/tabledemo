import React from 'react'
import {Column, Table} from 'react-virtualized'
import 'react-virtualized/styles.css'
import axios from 'axios'
import {Button} from 'antd'


class ReactVirtualized extends React.Component {
  constructor() {
    super()
    this.state = {
      data: []
    }
    axios.get('/test.json')
      .then(res => {
        let _max = []
    /*    for (let i = 0; i < 500; i++) {
          _max = [...res.data, ..._max]
        }
        console.log(_max.length)*/
        this.setState({data: res.data})

      })
  }

  render() {
    const {data} = this.state
    return (
      <Table
        width={800}
        height={900}
        headerHeight={20}
        rowHeight={30}
        rowCount={data.length}
        rowGetter={({index}) => data[index]}
      >
        <Column
          label='Mark'
          dataKey='mark'
          width={100}
          disableSort={true}
        />
        <Column
          width={400}
          label='ID'
          dataKey='id'
          cellRenderer={({cellData, columnData, rowData}) => {
            return (
              <Button
                onClick={() => {
                  console.log(cellData, columnData, rowData)
                }}
              >{cellData}</Button>
            )
          }}
        />
        <Column
          width={200}
          label='company_name'
          dataKey='company_name'
        />
        <Column
          width={200}
          label='updated_at'
          dataKey='updated_at'
        />
        <Column
          width={200}
          label='uuid'
          dataKey='uuid'
        />
        <Column
          width={200}
          label='owner'
          dataKey='owner'
        />


      </Table>
    )
  }
}

export default ReactVirtualized