import React from 'react'
import {Table, Button} from 'antd'
import axios from 'axios'

const columns = [
  {title: 'Full Name', width: 100, dataIndex: 'name', key: 'name', fixed: 'left'},
  {title: 'Age', width: 100, dataIndex: 'age', key: 'age', fixed: 'left'},
  {title: 'Column 1', dataIndex: 'address', key: '1'},
  {
    title: 'Action',
    key: 'operation',
    fixed: 'right',
    width: 100,
    render: () => <a href="#">action</a>,
  },
];

const _columns = [
  {title: 'Mark', width: 100, dataIndex: 'mark', key: 'mark', fixed: 'left', sorter: (a, b) => a.mark - b.mark},
  {title: 'id', dataIndex: 'id', key: 'id'},
  {title: 'company_name', dataIndex: 'company_name', key: 'company_name'},
  {title: 'uuid', dataIndex: 'uuid', key: 'uuid'},
  {
    title: 'battery_voltage',
    dataIndex: 'battery_voltage',
    key: 'battery_voltage',
    render: (text, record, index) => {

      return (
        <Button>{text}</Button>
      )
    },
    // fixed: 'right'
  },

]

const data = [{
  key: '1',
  name: 'John Brown',
  age: 32,
  address: 'New York Park',
}, {
  key: '2',
  name: 'Jim Green',
  age: 40,
  address: 'London Park',
}];

class AntdTable extends React.Component {

  constructor() {
    super()
    this.state = {
      data: []
    }
    axios.get('/test.json')
      .then(res => {
        console.log(res.data)
        this.setState({data: res.data})
      })
  }


  render() {
    return (
      <Table
        columns={_columns}
        dataSource={this.state.data}
        scroll={{x: 1300}}
        style={{width: 800}}
        pagination={false
          /*{
            pageSize: 50,
            showSizeChanger: true,
            showQuickJumper: true
          }*/
        }

      />
    )
  }
}

export default AntdTable
