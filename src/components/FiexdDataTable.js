import React from 'react'
import {Table, Column, Cell} from 'fixed-data-table'
import 'fixed-data-table/dist/fixed-data-table.min.css'
import axios from 'axios'
import store from '../mobx/appStore'
import {observer} from 'mobx-react'
import {Icon} from 'antd'

class MarkCell extends React.Component {
  render() {
    const {rowIndex, field, data, ...props} = this.props
    const mark = data[rowIndex][field]
    return (
      <Cell {...props}>
        <span>{mark}</span>
      </Cell>
    )
  }
}

//battery_voltage

class BatteryVoltageCell extends React.Component {
  render() {
    const {rowIndex, field, data, ...props} = this.props
    console.log(this.props)
    const battery_voltage = data[rowIndex][field]
    if (battery_voltage < 4) {

      return (
        <Cell {...props}>
          <Icon type="exclamation-circle-o"/>
        </Cell>
      )
    } else {
      return (
        <Cell {...props}>
          <Icon type="exclamation-circle"/>
        </Cell>
      )
    }

  }
}


@observer
class MyTable extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      myTableData: [
        {name: 'Rylan', email: 'Angelita_Weimann42@gmail.com'},
        {name: 'Amelia', email: 'Dexter.Trantow57@hotmail.com'},
        {name: 'Estevan', email: 'Aimee7@hotmail.com'},
        {name: 'Florence', email: 'Jarrod.Bernier13@yahoo.com'},
        {name: 'Tressa', email: 'Yadira1@hotmail.com'},
      ],
    }

    axios.get('/test.json')
      .then(res => {
        let _max = []
        for (let i = 0; i < 2; i++) {
          _max = [..._max, ...res.data]
        }
        console.log(_max.length)
        store.change('data', _max)

      })
  }

  render() {
    const data = store.data
    console.log(data)
    if (data.length > 0) {
      return (
        <div>
          <Table
            rowsCount={data.length}
            rowHeight={50}
            headerHeight={50}
            width={400}
            height={400}
          >
            <Column
              header={<Cell>Mark</Cell>}
              cell={
                <MarkCell
                  data={data}
                  field='mark'
                />
              }
              width={200}
            />
            <Column
              header={<Cell>battery_voltage</Cell>}
              cell={
                <BatteryVoltageCell
                  data={data}
                  field='battery_voltage'
                />
              }
              width={200}
            />

          </Table>
        </div>
      )
    } else {
      return <span>加载中...</span>
    }

  }
}

export default MyTable