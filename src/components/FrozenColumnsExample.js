import React from 'react'
import ReactDataGrid from 'react-data-grid'
import axios from 'axios'
import {Button, Icon} from 'antd'

class PercentCompleteFormatter extends React.Component {
  render() {
    const id = this.props.value
    return (
      <Button
        key={id}
        onClick={() => {
          console.log(id)
        }}
        type='primary'
      >{id}
      </Button>
    )
  }
}

class BatteryVoltageFormatter extends React.Component {
  render() {
    const v = this.props.value
    if (v > 4) {
      return (
        <Icon type='plus'/>
      )
    } else {
      return (
        <Icon type='delete'/>
      )
    }
  }
}

const _columns = [
  {
    key: 'uuid',
    name: 'UUID',
    width: 200,
    locked: true,
  },
  {
    key: 'company_name',
    name: 'company_name',
    width: 200,
  },
  {
    key: 'survive_time',
    name: 'survive_time',
    width: 200,
  }, {
    name: '设置',
    key: 'id',
    formatter: PercentCompleteFormatter
  },
  {
    key: 'battery_voltage',
    name: 'battery_voltage',
    width: 200,
    formatter: BatteryVoltageFormatter
  }


]


class FrozenColumnsExample extends React.Component {
  constructor() {
    super()
    this.state = {
      data: []
    }
    axios.get('/test.json')
      .then(res => {
        this.setState({data: res.data})
      })
  }

  render() {
    const a = i => {
      return this.state.data[i]
    }
    return (
      <ReactDataGrid
        columns={_columns}
        rowGetter={a}
        rowsCount={this.state.data.length}
        minHeight={500}
        minWidth={600}
      />
    )
  }
}

export default FrozenColumnsExample